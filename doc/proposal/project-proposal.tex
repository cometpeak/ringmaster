% CPSC 438 Final Project Proposal
%
%
% "It's not the size that matters. It's the jolt that it gives you" 
%    - Nikola Chonkov
%


\documentclass[letterpaper,12pt]{article}

% Packages
\usepackage{fixltx2e}
\usepackage[margin=2.54cm]{geometry}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amsthm}
\usepackage{paralist}
\usepackage{float}
\usepackage{graphicx}
\usepackage{algorithmic}
\usepackage{tikz}
\usepackage{mathpartir}
\usetikzlibrary{positioning,shadows,arrows}
\usepackage{listings}
\usepackage{caption}

\lstset{
    basicstyle=\footnotesize\ttfamily,
    numbers=left,
    numberstyle=\tiny,
    breaklines=true
    keywordstyle=\color{red},
    stringstyle=\color{white}\ttfamily,
    frame=b,
    showspaces=false,
    showtabs=false,
    xleftmargin=17pt,
    framexleftmargin=17pt,
    framexrightmargin=5pt,
    framexbottommargin=4pt,
    showstringspaces=false
}

\lstloadlanguages{
    C, C++, Java
}

\DeclareCaptionFont{white}{\color{white}}
\DeclareCaptionFormat{listing}{
    \colorbox[cmyk]{0.43, 0.35, 0.35,0.01}{
        \parbox{\textwidth}{\hspace{15pt}#1#2#3}
    }
}

\captionsetup[lstlisting]{
	format=listing,
    labelfont=white,
    textfont=white,
    singlelinecheck=false,
    margin=0pt,
    font={bf,footnotesize}
}

\begin{document}

\title{Project Proposal - Paxos symmetrization and survey of distributed agreement protocols}
\date{\today}
\author{CPSC 438 \\ David Z. Chen and Sherwin Yu}

\maketitle

\section{Introduction and Background}

Calvin is a deterministic shared-nothing distributed database system designed to scale nearly linearly, provide full ACID transactions. As in Thomson et al., a "deterministic" database system is one that enforces that the input state of the database system deterministically determines database state changes and output state. Thus, the states of replicas are guaranteed to stay consistent as long as they all receive the same input. As a result, the need for a post-transaction agreement protocol is eliminated.

Calvin partitions its data store across several nodes in a shared-nothing architecuture and maintains geographically-separate replicas of clusters to prevent a single point of failure. Calvin's transaction processing system is comprised of the \emph{sequencer} and \emph{scheduler}, both of which are partitioned across the nodes of a cluster. The sequencer handles the synchronous agreement of the input across all replicas, and the scheduler performs concurrency control and manages transaction execution and data access. The sequencer uses either an asynchronous replication scheme with leader election or synchronous implemented by ZooKeeper. While the asynchronous replication mode offers extremely low latency over ZooKeeper, failover and recovery using the asynchronous mode is extremely complex, especially in comparison to ZooKeeper.

ZooKeeper is an open source implementation of a variant of the Paxos distributed agreement protocol. Initially a sub-project of the Hadoop project, it is now a top-level project of the Apache Foundation. While ZooKeeper will allow more simple implementation of failover and recovery, current benchmarks on three replicas with four nodes per replica show that using ZooKeeper for distributed consensus in Calvin results in significantly more latency, especially if each of the replicas are in geographically-distant data centers. 

While ZooKeeper maybe the current \emph{de facto} open source standard for distributed agreement for small coordination data, it may not be best suited for the larger coordination loads of Calvin.

\subsection{Distributed consensus}

Paxos is an algorithm proposed by Lamport for distributed consensus in an asynchronous message-passing system. The original algorithm guarantees validity and agreeement with up to $n/2$ crash failures though with no guarantee (but high likelihood) of progress. The Paxos algorithm is in a way a generalization of two-phase commit. Processes are designated \emph{proposers}, \emph{accepters}, and \emph{learners}. The algorithm is as follows:

\begin{enumerate}
	\item A proposer sends a message $\mbox{prepare}(n)$ to each accepters with a unique proposal number $n$, which is often a timestamp. 
	\item If $n$ is greater than the highest numbered proposal the acceptor has already accepted:
	\begin{enumerate}
		\item Acceptor replies to the proposer with an $\mbox{ack}(n, v, n_v)$ where $v$ is the value of the highest numbered proposal it has accepted prior to $n$ and $n_v$ is the proposal number of that proposal.
	\end{enumerate}
	\item If proposer receives $\mbox{ack}$ from a majority of accepters:
	\begin{enumerate}
		\item The proposer selects a value $v$ out of the values received from proposers and initiates a second round of voting by sending a message $\mbox{accept}(n, v)$to each accepter.
	\end{enumerate}
	\item If accepter has not received $\mbox{prepare}(n')$ for some $n' > n$, it accepts $v$.
	\item If a majority of accepters accept $v$
	\begin{enumerate}
		\item Accepters send $v$ to each learner.
		\item $v$ becomes decision value for the protocol.
	\end{enumerate}
\end{enumerate}

According to a discussion by Junqueira and Reed, ZooKeeper in fact implements a variation of Paxos known as Multi-Paxos where a single stable proposer is assumed and we have a stream of multiple decision values transmitted over a FIFO channel, as guaranteed by TCP. Running an instance of Paxos for each decision value would incur too much overhead. ZooKeeper also cleverly switches leaders, which still gives accepters the illusion of a single logical proposer. As a result, the first round of voting is eliminated.

It is known that ZooKeeper is not optimized for larger data stores. Furthermore, ZooKeeper is \emph{asymmetric} as in it assumes a single stable proposer in order to eliminate the first round of voting. As a result, when a non-proposer replica attempts to propose an agreement value, it must make first make a request to the designated proposer before voting may begin. This extra step is thus another source of the latency observed with ZooKeeper.

\section{Scope of project}

The overarching goal of this project is to survey different variations of Paxos and possibly other distributed agreement protocols and analyze which protocols are better optimized for different workloads. First, we will attempt to "symmetrize" the variant of Multi-Paxos implemented by ZooKeeper. Then, we will implement a common server framework and client library in which we will implement several variations of Paxos and possibly other distributed agreement protocols. We will then benchmark the various protcols under different workload and conditions. An immediate objective is thus to achieve lower latency distributed agreement for Calvin sequencers. Finally, we plan to  publish our work after the completion of our project.

\subsection{Deliverables}

The deliverables of this project will consist of:

\begin{enumerate}
	\item A server framework for distributed consensus with implementations several different distributed agreement protocols.
	\item A client library that a distributed application like Calvin may use to interface with the server.
	\item Benchmarks of the implementations of various distributed agreement protocols under different workloads.
	\item A written report detailing our work, analyses, and results that will be refined for publication.
\end{enumerate}

\section{References}

\begin{enumerate}
	\item Thomson A. et al. "Calvin: Fast Distributed Transactions for Partitioned Database Systems." (2012).
	\item http://www.mail-archive.com/zookeeper-user@lists.sourceforge.net/msg00041.html
	\item Lamport, L. "Fast Paxos." http://research.microsoft.com/apps/pubs/default.aspx?id=64624
\end{enumerate}

\end{document}
