
\documentclass[letterpaper,12pt]{article}

% Packages
\usepackage{fixltx2e}
\usepackage[margin=2.54cm]{geometry}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amsthm}
\usepackage{paralist}
\usepackage{float}
\usepackage{graphicx}
\usepackage{algorithmic}
\usepackage{tikz}
\usepackage{mathpartir}
\usetikzlibrary{positioning,shadows,arrows}
\usepackage{listings}
\usepackage{caption}

\lstset{
    basicstyle=\footnotesize\ttfamily,
    numbers=left,
    numberstyle=\tiny,
    breaklines=true
    keywordstyle=\color{red},
    stringstyle=\color{white}\ttfamily,
    frame=b,
    showspaces=false,
    showtabs=false,
    xleftmargin=17pt,
    framexleftmargin=17pt,
    framexrightmargin=5pt,
    framexbottommargin=4pt,
    showstringspaces=false
}

\lstloadlanguages{
    C, C++, Java
}

\DeclareCaptionFont{white}{\color{white}}
\DeclareCaptionFormat{listing}{
    \colorbox[cmyk]{0.43, 0.35, 0.35,0.01}{
        \parbox{\textwidth}{\hspace{15pt}#1#2#3}
    }
}

\captionsetup[lstlisting]{
    format=listing,
    labelfont=white,
    textfont=white,
    singlelinecheck=false,
    margin=0pt,
    font={bf,footnotesize}
}

\begin{document}

\title{Implementation notes}
\date{\today}
\author{CPSC 438 \\ David Z. Chen, Sherwin Yu}

\maketitle

\section{ZooKeeper Design Analysis}

Isn't "zookeeper" already one word? Why the camel case?

\subsection{Data Serialization}

Jute is the serialization format and library used by ZooKeeper, thus it is along the same lines as Google Protocol Buffers (which is used by Calvin) and Thrift. Jute appears to be mostly identical to Hadoop Record. In fact, the package.html file is titled Hadoop Record.\footnote{Located in \texttt{zookeeper/src/main/java/main/org/apache/jute}}  The main difference between Jute and Hadoop Record "proper" is that Jute comes with C bindings since ZooKeeper provides C bindings.\footnote{This is described in a certain issue on the ZooKeeper issue tracker (https://issues.apache.org/jira/browse/ZOOKEEPER-102)}

In any event, it appears that Jute isn't really used outside of Hadoop and ZooKeeper, especially in comparison to Protocol Buffers and Thrift. According to some, even the ZooKeeper folk are planning to abandon Jute.\footnote{See http://www.lexemetech.com/2008/07/rpc-and-serialization-with-hadoop.html} 

Calvin uses Protocol Buffers to serialize messages where are then passed to the ZooKeeper client library, which then encapsulates them with Jute to send to the ZooKeeper server.

\textbf{Tl;DR:} For Ringmaster, we will be using Protocol Buffers.\footnote{See the protobuf documentation, especially the scalar value types table, for important information about mapping to language types and, *ahem*, string encodings (http://developers.google.com/protocol-buffers/docs/proto)} The client will use Protobuf-C since it will be implemented in C. 

\subsection{Properties}

According to the ZooKeeper paper, ZooKeeper implements a \emph{universal object}, i.e. it can achieve consensus for any number of processes. In ZooKeeper, znodes are \emph{wait-free} objects and arranged hierarchically. For operations, the following ordering properties are guaranteed:

\begin{description}
	\item[A-Linearizable writes and updates:] all write and update operations are serializable and respect precedence.
	\item[FIFO client ordering:] all requests sent from a given client are executed in the order that they were sent.
\end{description}

The paper defines \emph{asynchronous linearizability} or \emph{A-linearizability} as allowing a client to have multiple outstanding asynchronous operations and preserve FIFO order for those outstanding operations. According to the paper, A-linearizability also satisfies linearizability. \textbf{Only writes and updates are A-linearizable, and reads are processed locally for each ZooKeeper replica (instance).}

\subsection{Consensus}

Clients (though the client library) submit requests to only one ZooKeeper server. Read requests are processed by the local server. Writes and updates require executing the agreement protocol.

The consensus protocol used by ZooKeeper is called \textbf{Zab (ZooKeeper Atomic Broadcast)}. One server is designated a \emph{leader} and all others are \emph{followers}. A key difference between Zab and Paxos is that Zab has stronger ordering guarantees than classic Paxos by guaranteeing that all messages sent by the leader are delivered in the order that they were sent. A new leader is only chosen if it is agreed upon by a majority of the servers, and all messages sent by previous leaders are delivered to the newly established leader before the new leader broadcasts its own requests.

\textbf{This is where we may make an optimization.} This is also where the asymmetry that Alex was talking about is.

\textbf{By the way}, Section 4.4 of the ZooKeeper paper is extremely useful and may warrant a re-read.

\subsection{Miscellaneous}

Apparently the ZooKeeper client library is built as 2 shared objects: a single-threaded library and a multi-threaded one. Calvin uses the multi-threaded ZooKeeper client library.

\section{Ringmaster Specifications}

We should implement Ringmaster to have mostly the same properties of ZooKeeper, especially the ordering guarantees. Our framework also needs to be modular enough so that different consensus protocols may be swapped in and out.

\subsection{Client-facing API}

This is the API that the Ringmaster server provides to the client library (as opposed to the client application that uses Ringmaster using the client library). Here, I will use the term \emph{object} instead of znode because nodes are machines. See the file \texttt{src/proto/client.proto} for the details of each message type.

\begin{description}
	\item[\texttt{ConnectionResponse connect(ConnectionRequest)}] - connect to server and create session with client.
	\item[\texttt{CreateResponse create(path, data)}] - create object with specified path, storing data.
	\item[\texttt{DeleteResponse delete(path, version)}] - delete object with specified path if object's version matches specified version.
	\item[\texttt{ExistsResponse exists(path, watch)}] - check if object with specified path exists. If watch is set to true, set a watch for client on the object. If object does not exist, watch is still set and client is notified when object is created.
	\item[\texttt{GetDataResponse getData(path, watch)}] - retrieve metadata and data associated with object with specified path. If watch is set to true, then watch is set for client on the object if object exists. Client is notified when object is modified.
	\item[\texttt{SetDataResponse setData(path, data, version)}] - set the data for object with specified path if version of object matches specified version.
	\item[\texttt{GetChildrenResponse getChildren(path)}] - gets keys of all children of object with specified path.
	\item[\texttt{SyncResponse sync()}] - wait for all updates to the server the client is connected to finish.
	\item[\texttt{DisconnectResopnse disconnect(DisconnectRequest)}] - disconnect with server and delete session.
\end{description}

\subsection{User-facing API}

This is the API that the Ringmaster client library provides to the client application.

\section{Roadmap}

\begin{enumerate}
	\item Agree upon client-server interface and messages.
	\item Implement server framework with Zab - this essentially gives us a stripped-down version of ZooKeeper.
	\item Implement client-side library.
	\item Implement a "symmetric" atomic broadcast/consensus protocol and one other. Some papers we should look into (see lit directory):
	\begin{enumerate}
		\item Improving Fast Paxos
		\item Multi-coordinated atomic broadcast
	\end{enumerate}
\end{enumerate}

\end{document}
