// Data store

package store

import (
	"fmt"
	"bytes"
)

type Store struct {
	size *int32
	head *Entry
	tail *Entry
}

type Entry struct {
	seqnum int32
	data string
	next *Entry
	prev *Entry
}

func Append(store *Store, seqnum int32, data string) {

	entry := new(Entry)
	entry.seqnum = seqnum
	entry.data = data
	entry.prev = nil
	entry.next = nil

	if store.size == 0 {
		store.head = entry
		store.tail = entry
	} else {
		tail := store.tail
		tail.next = entry
		entry.prev = tail
		store.tail = entry
	}

	store.size += 1
}

func All(store *Store) string {

	buffer := bytes.NewBufferString("")

	for entry := store.head; entry != nil; entry = entry.next {
		fmt.Fprint(buffer, entry.data)
	}

	return string(buffer.Bytes())
}

func NewStore() *Store {
	s := new(Store)

	s.head = nil
	s.tail = nil
	s.size = 0

	return s
}
