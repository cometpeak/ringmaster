package server

import (
	"fmt"
	"io"
	"net"
	// "os"
	"errors"
	// "code.google.com/p/goprotobuf/proto"
	"log"
	"strconv"
	"time"
)

type Network interface {
	Setup(config []string)
	LocalPort() int
	// LocalAddr() Address
	Send(dest Address, payload string)
	Broadcast(payload string)
	Incoming() chan Message
	Peers() []Address
}

type NetworkImpl struct {
	localAddr        Address
	localPort        int
	incoming         chan Message
	outgoing         chan Message
	welcomeSocket    net.Listener
	peersMapOutgoing map[Address]net.Conn
	peersMapIncoming map[Address]net.Conn
}

/* Setup initializes the Network from its zero-value
* > read in the config (which should specifify the destination addresses
* > establish the TCP connections with peers
* > initialize the fields
* > do everyting necessary to make sure Send, Broadcast, and Incoming() work properly
* > start the go routine that writes outgoing messagse from the channel into the TCP connection
* > start the go routines that read from each TCP connection
*
 */

func S(c net.Conn) string {
	return fmt.Sprintf("%s->%s", c.LocalAddr(), c.RemoteAddr())
}

func SL(c net.Listener) string {
	return fmt.Sprintf("%s", c.Addr())
}

func (ntwk *NetworkImpl) acceptConnections() {
	var err error
	ntwk.welcomeSocket, err = net.Listen("tcp", ":"+strconv.Itoa(ntwk.localPort))
	log.Printf("listening on %s", SL(ntwk.welcomeSocket))

	if err != nil {
		log.Fatal("can't listen ", err)
	}

	//accept connections
	for {
		conn, err := ntwk.welcomeSocket.Accept()
		if err != nil {
			log.Fatal("error accepting socket", conn.LocalAddr())
		}
		log.Println("Accept(), conn=%v", S(conn))

		// Handle  new  connection  in  own  go  routine 
		go ntwk.readIncoming(conn)
	}
}

func (ntwk *NetworkImpl) dialConnections(addrs []string) {
	for _, a := range addrs {
		addr := Address(a)
		err := errors.New("placeholder")
		for err != nil {
			time.Sleep(1 * time.Second)
			fmt.Printf("attempting to dial: %s\n", a)
			ntwk.peersMapOutgoing[addr], err = net.Dial("tcp", a) //, 15 * time.Second)
		}
		if err != nil {
			log.Fatal("local addr invalid ", err)
		}
		log.Println("Successfully dialed: %s", S(ntwk.peersMapOutgoing[addr]))
	}
	log.Println("All connections established.")
}

// TODO(syu): implement
// config[0] is the local port
// config[i] is of the form hostname:port
func (ntwk *NetworkImpl) Setup(config []string) {
	var err error
	ntwk.localPort, err = strconv.Atoi(config[0])
	ntwk.peersMapOutgoing = make(map[Address]net.Conn)
	if err != nil {
		log.Fatal("local addr invalid", err)
	}

	// accept connections
	go ntwk.acceptConnections()

	// Attempt to dial connections
	ntwk.dialConnections(config[1:])

	// write outgoing messages to TCP connections
	go ntwk.writeOutgoing()

	for {
		time.Sleep(1 * time.Second)
	}
}

func (ntwk *NetworkImpl) LocalPort() int {
	return ntwk.localPort
}

func (ntwk *NetworkImpl) Send(dest Address, payload string) {
	msg := Message{ntwk.localAddr, dest, payload}
	ntwk.outgoing <- msg

}

func (ntwk *NetworkImpl) Broadcast(payload string) {
	for dest, _ := range ntwk.peersMapOutgoing {
		msg := Message{ntwk.localAddr, dest, payload}
		ntwk.outgoing <- msg
	}
}

/* Takes messages from the outgoing channel, 
Looks up the corresponding TCP connection for the Dest
from peersMapOutgoing and writes the message to the conn.
*/
//TODO(syu): test me
func (ntwk *NetworkImpl) writeOutgoing() {
	for msg := range ntwk.outgoing {
		println("writing msg", msg.Dest)
		conn := ntwk.peersMapOutgoing[msg.Dest]

		// First, write the length field

		var length int32 = int32(len(msg.Payload)) // len(string) returns length in bytes... ha ha! screw you unicode!
		// TODO(syu) NO IDEA IF THIS WORKS. TEST ME. I HOPE IT WORKSK
		bytearr := [4]byte{}
		bytearr[0] = byte((uint32(length) & 0xff000000) >> 24)
		bytearr[1] = byte((length & 0x00ff0000) >> 16)
		bytearr[2] = byte((length & 0x0000ff00) >> 8)
		bytearr[3] = byte((length & 0x000000ff) >> 0)

		conn.Write(bytearr[:])
		conn.Write([]byte(msg.Payload))
	}
}

func (ntwk *NetworkImpl) readIncoming(conn net.Conn) {
	log.Println("Reading incoming", S(conn))

	for {
		msg := Message{}
		msg.Src = Address(conn.RemoteAddr().String())
		msg.Dest = Address("localhost:" + strconv.Itoa(ntwk.LocalPort()))
		lengthField := make([]byte, 4)
		_, err := io.ReadFull(conn, lengthField[:])
		if err != nil {
			log.Fatal("error reading length field", err)
		}

		var length uint32
		length |= uint32(lengthField[0]) << 24
		length |= uint32(lengthField[1]) << 16
		length |= uint32(lengthField[2]) << 8
		length |= uint32(lengthField[3]) << 0
		log.Println("incomingp packet has length", length)

		// TODO(syu): Actually read in the data properly
		payloadBuf := make([]byte, length)

		_, err = io.ReadFull(conn, payloadBuf)
		if err != nil {
			log.Fatal("error reading payload", err)
		}

		msg.Payload = string(payloadBuf)

		log.Println("Read payload:", msg.Payload)
		log.Println("Pushing message", msg)
		ntwk.incoming <- msg

	}
}

// TODO(syu):
// Hmm, not sure exactly how to do this one; need to multiplex a whole bunch of incomoing TCP connections. Probably a select on TCP connections? Or, probably, need a separate

// readIncomoing(con net.Conn) for each connection, and Setup() will kick off n goroutines. Look into this.
// ultimately, readIncoming reads from a net.Conn and pushes a Message to the incoming channel

/* 
* Used to get a hold of the incoming Message channel from
* //TODO(syu): is this necessary?
*
 */
func (ntwk *NetworkImpl) Incoming() chan Message {
	return ntwk.incoming
}

// Simply returns the list of peers as an slice of Addresses
func (ntwk *NetworkImpl) Peers() []Address {
	peers := make([]Address, len(ntwk.peersMapOutgoing))
	i := 0
	for k, _ := range ntwk.peersMapOutgoing {
		peers[i] = k
		i++
	}
	return peers
}
