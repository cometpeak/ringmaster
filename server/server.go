// Echo server to get my bearings straight
//
// TODO(syu): Move network/networkimpl into its own file (package?)

package server

import (
  // "fmt"
  // "io"
  // "net"
  // "os"
  // "errors"
  // "code.google.com/p/goprotobuf/proto"
  // "log"
  // "strconv"
  // "time"
)

type Server struct {
  network Network
}

func (serv *Server) Start() {
  // TODO(syu): Figure out what we want to pass as config
  var config []string
  serv.network.Setup(config)
  go serv.receiveMessages()
}

func (serv *Server) receiveMessages() {
}

type Address string

// sample usage:
// for 

/* 
* Messages should always have a Source field.
* Messages should have positive Dest field only if it is a non-broadcast message
* Network.Send(message, destination) specifies the address and
* Network.Broadcast(message) doesn't
*/
type Message struct {
  Src Address
  Dest   Address
  Payload   string
}

/*
func Handle(packet TcpPacket) {
  packet.con.Write([]byte(packet.data))

  fmt.Printf("From socket: %w data: '%s'\n", packet.con, packet.data)
}

func ListenSocket(channel chan TcpPacket, con net.Conn) {
  fmt.Printf("=== New Connection received from: %s\n", con.RemoteAddr())

  data := make([]byte, 1024)
  defer con.Close()

  for {
    n, error := con.Read(data)

    switch error {
    case nil:
      packet := TcpPacket{
        string(data[0:n]),
        con,
      }
      channel <- packet
    case io.EOF:
      fmt.Printf("Warning: End of data reached: %s\n", error)
      return
    default:
      fmt.Printf("Error: Reading data: %s\n", error)
      return
    }
  }
}

func Listen(channel chan TcpPacket, laddr string) {
  fmt.Println("Listen started")

  lis, error := net.Listen("tcp", laddr)
  defer lis.Close()

  if error != nil {
    fmt.Printf("Error creating listener: %s\n", error)
    os.Exit(1)
  }

  for {
    con, error := lis.Accept()
    if error != nil {
      fmt.Printf("Error: Accepting data: %s\n", error)
      os.Exit(2)
    }

    go listen_socket(channel, con)
  }
}

func Start(host string, port, string, store *Store) {
  fmt.Println("Initiating server... (Ctrl-C to stop)")

  laddr := host + ":" + port
  channel := make(chan TcpPacket)

  go Listen(channel, laddr)

  for {
    go Handle(<-channel)
  }
}

*/
