package consensus

import (
	"fmt"
	"log"
	"ringmaster/server"
	"strconv"
	"strings"
	"time"
)

/* proposals last up to 2 seconds! LOL! */
const ProposalTTL int64 = 2

type Proposal struct {
	n           int
	v           string
	highestN    int
	highestV    string
	numPromised int
	numAccepted int
	time        int64
}

type ClassicPaxos struct {
	id              int
	numPeers        int
	currentProposal Proposal
	lastPromisedN   int
	acceptedN       int
	acceptedV       string
	ntwk            *server.NetworkImpl
}

func (paxos *ClassicPaxos) OnMessage(from server.Address, msg string) {
	parts := strings.Split(msg, " ")

	/*
			  Messages have the following format:
		          PREPARE n
			  PROMISE n nOld vOld
		          ACCEPT n v
		          ACCEPTED n v

	*/

	// if we don't have at least 2 (command and n), it's an error
	if len(parts) < 2 {
		log.Fatal("paxos received message with only one component")
	}
	n, err := strconv.Atoi(parts[1])

	if err != nil {
		log.Fatal("paxos received message with invalid N")
	}

	// if we have 3 (command n v), v becomes non-empty. otherwise, v = ""
	// this case neesd to be considered for PROMISE messages
	var v string = ""

	switch parts[0] {
	case "PREPARE":
		paxos.OnPrepare(from, n)
	case "PROMISE":
		if len(parts) != 4 {
			log.Fatal("paxos PROMISE incorrect message args")
		}
		nOld, err := strconv.Atoi(parts[2])
		if err != nil {
			log.Fatal("paxos PROMISE failed nOld conversion: ", err)
		}

		// if the sender has already promised someting
		if nOld != 0 {
			v = parts[3]
		}
		paxos.OnPromise(from, n, nOld, v)
	case "ACCEPT":
		if len(parts) != 3 {
			log.Fatal("paxos ACCEPTK incorrect message args")
		}
		v = parts[2]
		paxos.OnAccept(from, n, v)
	case "ACCEPTED":
		if len(parts) != 3 {
			log.Fatal("paxos ACCEPTK incorrect message args")
		}
		v = parts[2]
		paxos.OnAccepted(from, n, v)
	}

}

func (paxos *ClassicPaxos) sendPrepare(n int) {
	msg := fmt.Sprintf("PREPARE %d", n)
	paxos.ntwk.Broadcast(msg)
}

// v is "" if no value previously accepted
// When promising, need to include both the proposal number we're replying to (n) and
// the highest proposal number we've accepted thus far (nOld) and it's value (v)
func (paxos *ClassicPaxos) sendPromise(to server.Address, n int, nOld int, v string) {
	msg := fmt.Sprintf("PROMISE %d %d %s", n, nOld, v)
	paxos.ntwk.Send(to, msg)
}

func (paxos *ClassicPaxos) sendAccept(n int, v string) {
	msg := fmt.Sprintf("ACCEPT %d %s", n, v)
	paxos.ntwk.Broadcast(msg)
}

func (paxos *ClassicPaxos) sendAccepted(to server.Address, n int, v string) {
	msg := fmt.Sprintf("ACCEPTED %d %s", n, v)
	paxos.ntwk.Send(to, msg)
}

func (paxos *ClassicPaxos) NextN() int {
	if paxos.acceptedN > paxos.lastPromisedN {
		return paxos.acceptedN + 1
	}
	return paxos.lastPromisedN + 1
}

func (paxos *ClassicPaxos) hasPromiseMajority() bool {
	return (paxos.currentProposal.numPromised > paxos.numPeers/2)
}

func (paxos *ClassicPaxos) hasAcceptMajority() bool {
	return (paxos.currentProposal.numAccepted > paxos.numPeers/2)
}

func (paxos *ClassicPaxos) proposalExpired() bool {
	if paxos.currentProposal.time == 0 {
		return true
	}

	return time.Now().Unix()-paxos.currentProposal.time > ProposalTTL
}

func (paxos *ClassicPaxos) resetCurrentProposal() {
	paxos.currentProposal = Proposal{
		n:           0,
		v:           "",
		highestN:    0,
		highestV:    "",
		numPromised: 0,
		numAccepted: 0,
		time:        0,
	}
}

/*
 * Received Prepare(n) message
 */
func (paxos *ClassicPaxos) OnPrepare(from server.Address, n int) {
	if n < paxos.lastPromisedN {
		return
	}

	paxos.lastPromisedN = n
	// CHANGED(syu): added nOld as third argument:
	// paxos.sendPromise(from, n, paxos.acceptedV)
  nOld := paxos.currentProposal.highestN
	paxos.sendPromise(from, n, nOld, paxos.acceptedV)
}

/*
 * Received Promise(n, v) message
 */
func (paxos *ClassicPaxos) OnPromise(from server.Address, n int, nOld int, v string) {
	if n != paxos.currentProposal.n {
		return
	}

	paxos.currentProposal.numPromised++
	if nOld > paxos.currentProposal.highestN {
		paxos.currentProposal.highestN = nOld
		paxos.currentProposal.highestV = v
	}

	if paxos.proposalExpired() && !paxos.hasPromiseMajority() {
		paxos.Propose(v)
		return
	}

	if paxos.hasPromiseMajority() {
		paxos.sendAccept(n, paxos.currentProposal.highestV)
	}
}

/*
 * Received Accept(n) message
 */
func (paxos *ClassicPaxos) OnAccept(from server.Address, n int, v string) {
	if n < paxos.lastPromisedN {
		return
	}

	paxos.acceptedN = n
	paxos.acceptedV = v

	// XXX Update server store with new value

	paxos.sendAccepted(from, n, v)
}

/*
 * Received Accepted(n) message
 */
func (paxos *ClassicPaxos) OnAccepted(from server.Address, n int, v string) {
	if n != paxos.currentProposal.n {
		return
	}

	paxos.currentProposal.numAccepted++
	if paxos.proposalExpired() && !paxos.hasAcceptMajority() {
		paxos.Propose(v)
		return
	}

	if paxos.hasAcceptMajority() {
		// XXX Report results

		paxos.resetCurrentProposal()
	}
}

/*
 * Propose value
 */
func (paxos *ClassicPaxos) Propose(v string) {
	n := paxos.NextN()

	paxos.currentProposal = Proposal{
		n:           n,
		v:           v,
		highestN:    0,
		highestV:    "",
		numPromised: 1,
		numAccepted: 1,
		time:        time.Now().Unix(),
	}

	paxos.sendPrepare(n)
}

/*
 * Initializer
 */
func (paxos *ClassicPaxos) Setup(id int, numPeers int, ntwk *server.NetworkImpl) {
	paxos.resetCurrentProposal()
	paxos.numPeers = numPeers
	paxos.ntwk = ntwk
	paxos.lastPromisedN = 0
	paxos.acceptedN = 0
	paxos.acceptedV = ""
}
