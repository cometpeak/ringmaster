package consensus

type Consensus interface {
	NextN() int
	Propose(v string)
	OnMessage(message string)
}

