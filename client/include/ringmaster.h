#ifndef RINGMASTER_H_
#define RINGMASTER_H_

#define RG_ERRNO_MAP(XX) \
	XX(0,  OK) \
	XX(1,  ECONNLOSS) \
	XX(2,  ESYSTEM) \
	XX(3,  EMARSHAL) \
	XX(4,  EUNMARSHAL) \
	XX(5,  EBADARG) \
	XX(6,  ETIMEOUT) \
	XX(7,  EAPI) \
	XX(8,  EPROPOSAL) \
	XX(9,  ESEQNUM) \
	XX(10, ESERVER)

#define RG_ERRNO_GEN(val, name) RG_##name = val,
enum {
	RG_ERRNO_MAP(RG_ERRNO_GEN)
	RG_EUNKNOWN
};
#undef RG_ERRNO_GEN

static char *rg_err_strings[] = {
	[RG_OK]         = "Success",
	[RG_ECONNLOSS]  = "Connection loss",
	[RG_ESYSTEM]    = "System error",
	[RG_EMARSHAL]   = "Marshalling error",
	[RG_EUNMARSHAL] = "Unmarshalling error",
	[RG_EBADARG]    = "Invalid argument",
	[RG_ETIMEOUT]   = "Operation timed out",
	[RG_EAPI]       = "API error",
	[RG_EPROPOSAL]  = "Proposal failed",
	[RG_ESEQNUM]    = "Invalid sequence number",
	[RG_ESERVER]    = "Server error",
	[RG_EUNKNOWN]   = "Unknown error"
};
static inline char *ringmaster_strerror(err)
{
	return rg_err_strings[err];
}

/*
 * Client state
 */
enum RgState {
	RG_STATE_DISCONNECTED,
	RG_STATE_CONNECTING,
	RG_STATE_CONNECTED
};

/*
 * Operations
 */
#define RG_OP_MAP(XX) \
	XX(0, RETR) \
	XX(1, APPEND) \
	XX(2, DISCONNECT)

#define RG_OP_GEN(val, name) RG_OP_##name = val,
enum RgOp {
	RG_ERRNO_MAP(RG_OP_GEN)
	RG_NOPS
};
#undef RG_OP_GEN

/*
 * Handle object.
 */
typedef struct ringmaster_handle RgHandle;

/*
 * Structure to represent status information.
 */
typedef struct ringmaster_stat RgStat;
struct ringmaster_stat {
	int32_t seqnum;
	int32_t len;
};

/*
 * Structure to represent an Ringmaster server API call operation
 */
typedef struct ringmaster_op RgOp;
struct ringmaster_op {
	enum {
		RG_OP_RETR   = 0,
		RG_OP_APPEND = 1,
	} type;

	union {
		struct {
			int seqnum;
		} op_retr;

		struct {
			char *buffer;
			size_t len;
		} op_append;
	};
};

/**
 * Function signature for callback for exists(), create(), and set().
 *
 * @param retval
 * @param stat
 * @param callback_data
 */
typedef void (*RgStatCallback)(int retval, const RgStat *stat, 
                               const void *callback_data);

/**
 * Function signature for callback for get().
 *
 * @param retval
 * @param value
 * @param len
 * @param stat
 * @param callback_data
 */
typedef void (*RgDataCallback)(int retval, const char *value, int len,
			       const void *callback_data);

/**
 * Initializes a Ringmaster session and attempt to connect to host. This is
 * done asynchronously.
 *
 * @param host
 * @param timeout
 * @return pointer to an RgHandle object
 */
RgHandle *ringmaster_init(const char *host, int timeout);

/**
 * Append entry to log
 * Operation: append(buffer)
 *
 * @param handle
 * @param buffer
 * @param len
 * @param callback
 * @param callback_data
 * @return
 */
int ringmaster_append(RgHandle *handle, const char *buffer, size_t len,
                      RgStatCallback callback, void *callback_data);

/**
 * Retrieve the entire log store as a string starting at given log sequence
 * number. If sequence number is 0, then retrieve the entire log.
 *
 * @param handle
 * @param key
 * @param callback
 * @param callback_data
 * @return
 */
int ringmaster_retr(RgHandle *handle, int seqnum, RgDataCallback callback,
                   void *callback_data);

/**
 *
 * @param handle
 * @return
 */
int ringmaster_close(RgHandle *handle);

#endif /* RINGMASTER_H_ */
