#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "client.pb-c.h"

int receive(char **buffer)
{
	char *s = "Hello world";
	int len = strlen(s);
	char *buf;
	char *request_buf;
	int packed_len;

	RequestHeader request_header = REQUEST_HEADER__INIT;
	AppendRequest append_request = APPEND_REQUEST__INIT;

	request_header.type = REQUEST_HEADER__TYPE__APPEND;

	printf("header length: %d\n", 
			request_header__get_packed_size(&request_header));
	printf("request type: %d\n",
			REQUEST_HEADER__TYPE__APPEND);

	append_request.requestheader = &request_header;
	append_request.len = len;

	buf = (char *) malloc((len) * sizeof(*buf));

	strncpy(buf, s, len);
	append_request.data.len = len;
	append_request.data.data = (uint8_t *) buf;

	packed_len = append_request__get_packed_size(&append_request);
	request_buf = (char *) malloc(packed_len * (sizeof(*request_buf)));

	append_request__pack(&append_request, request_buf);
	free(buf);

	*buffer = request_buf;
	return packed_len;
}

int unpackheader(char *buffer, int len)
{
	RequestHeader *header;

	header = request_header__unpack(NULL, 4, buffer);
	assert(header != NULL);
	printf("Request type: %d\n", header->type);

	request_header__free_unpacked(header, NULL);
}


int unpack(char *buffer, int len)
{
	AppendRequest *request;
	RequestHeader *header;

	request = append_request__unpack(NULL, len, buffer);
	header = request->requestheader;

	printf("Append request:\n");
	printf("  Header:\n");
	printf("    Request type: %d\n", header->type);
	printf("  Len: %d\n", request->len);
	printf("  Data:\n");
	printf("    Len: %d\n", request->data.len);

	char *s = (char *) malloc((request->data.len + 1) * sizeof(*s));
	s[request->data.len] = '\0';
	strncpy(s, request->data.data, request->data.len);

	printf("    Data: %s\n", s);

	append_request__free_unpacked(request, NULL);
}

int main()
{
	char *buffer;
	int len;

	len = receive(&buffer);

	unpackheader(buffer, len);
	unpack(buffer, len);

	free(buffer);

	return 0;
}
