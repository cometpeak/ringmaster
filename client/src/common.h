#ifndef COMMON_H_
#define COMMON_H_

#include <stdint.h>

#ifndef typeof
#  define typeof __typeof__
#endif

#define offsetof(TYPE, MEMBER) \
	((size_t) &((TYPE *) 0)->MEMBER)

/**
 * container_of - cast a member of a structure out to the containing structure
 *
 * @param ptr - the pointer to the member.
 * @param type - the type of the contaer struct this is embedded in.
 * @param member - the name of the member within the struct.
 */
#define container_of(ptr, type, member) ({                      \
        const typeof( ((type *)0)->member ) *__mptr = (ptr);    \
        (type *)( (char *)__mptr - offsetof(type,member) );})

/*
 * Efficient min and max operations
 */
#define MIN(_a, _b)                                             \
({                                                              \
        typeof(_a) __a = (_a);                                  \
        typeof(_b) __b = (_b);                                  \
        __a <= __b ? __a : __b;                                 \
})

#define MAX(_a, _b)                                             \
({                                                              \
        typeof(_a) __a = (_a);                                  \
        typeof(_b) __b = (_b);                                  \
        __a >= __b ? __a : __b;                                 \
})

// Rounding operations (efficient when n is a power of 2)
// Round down to the nearest multiple of n
#define ROUNDDOWN(a, n)                                         \
({                                                              \
        uint32_t __a = (uint32_t) (a);                          \
        (typeof(a)) (__a - __a % (n));                          \
})

// Round up to the nearest multiple of n
#define ROUNDUP(a, n)                                           \
({                                                              \
        uint32_t __n = (uint32_t) (n);                          \
        (typeof(a)) (ROUNDDOWN((uint32_t) (a) + __n - 1, __n)); \
})

#endif
