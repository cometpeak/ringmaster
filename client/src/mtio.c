#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/epoll.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <pthread.h>

#include <ringmaster.h>

#include "common.h"
#include "client.h"
#include "debug.h"
#include "list.h"
#include "atomic.h"
#include "message.h"
#include "mtio.h"

static int32_t xid_counter;

struct buffer *buffer_new(int len)
{
	struct buffer *buf;

	buf = (struct buffer *) malloc(sizeof(*buf));
	if (buf == NULL)
		return NULL;

	buf->length = len;
	buf->offset = 0;
	buf->buffer = (char *) malloc(len);
	if (buf->buffer == NULL) {
		free(buf);
		return NULL;
	}

	return buf;
}

void buffer_free(struct buffer *buf)
{
	free(buf->buffer);
	free(buf);
}

static void completion_queue_lock(struct completion_queue *queue)
{
	pthread_mutex_lock(&queue->lock);
}

static void completion_queue_unlock(struct completion_queue *queue)
{
	pthread_mutex_unlock(&queue->lock);
}

static __attribute__((unused)) void completion_queue_notify(struct completion_queue *queue)
{
	pthread_cond_broadcast(&queue->cond);
}

static void buffer_queue_lock(struct buffer_queue *queue)
{
	pthread_mutex_lock(&queue->lock);
}

static void buffer_queue_unlock(struct buffer_queue *queue)
{
	pthread_mutex_unlock(&queue->lock);
}

static int set_nonblock(int fd)
{
	int flags;

	flags = fcntl(fd, F_GETFL, 0);
	if (flags < 0) {
		perror("fcntl");
		return -1;
	}

	if (flags & O_NONBLOCK)
		return 0;

	if (fcntl(fd, F_SETFL, O_NONBLOCK | flags) < 0) {
		perror("fcntl");
		return -1;
	}

	return 0;
}

static void drain(int fd)
{
	char buf[128];

	while (read(fd, buf, sizeof(buf)) == sizeof(buf))
		;
}

static void event_process(RgHandle *handle)
{

}

static void *event_main(void *h)
{
	RgHandle *handle = (void *) h;
	struct completion_queue *inbound_completions;

	inbound_completions = &(handle->inbound_completions);

	/* Event dispatch thread loop. */
	while (handle->close_requested == 0) {
		pthread_mutex_lock(&(inbound_completions->lock));
		while (list_empty(inbound_completions->list)) {
			pthread_cond_wait(&(inbound_completions->cond),
			                  &(inbound_completions->lock));
		}
		pthread_mutex_unlock(&(inbound_completions->lock));
		event_process(handle);
	}
}

int io_connect(RgHandle *handle)
{
	int socket_fd;
	int retval;
	int enable_tcp_nodelay = 1;

	if (handle->socket_fd != -1) {
		assert(handle->state == RG_STATE_CONNECTED);
		return 0;
	}
	
	enable_tcp_nodelay = 1;

	socket_fd = socket(handle->addr.sa_family, SOCK_STREAM, 0);
	if (socket_fd < 0) {
		perror("socket");
		return -1;
	}

	retval = setsockopt(socket_fd, IPPROTO_TCP, TCP_NODELAY, 
	                    &enable_tcp_nodelay, 
			    sizeof(enable_tcp_nodelay));
	if (retval != 0) {
		fprintf(stderr, "io_connect: cannot set TCP no delay.\n");
	}

	if (set_nonblock(socket_fd) < 0) {
		fprintf(stderr, "Cannot set socket_fd to nonblocking");
		return -1;
	}

	handle->socket_fd = socket_fd;

	retval = connect(socket_fd, &(handle->addr), sizeof(struct sockaddr));
	if (retval == -1) {
		if (errno == EWOULDBLOCK || errno == EINPROGRESS) {
			handle->state = RG_STATE_CONNECTING;
		} else {
			perror("connect");
			return -1;
		}
	}

	return 0;
}

static int io_recv(int socket_fd, struct buffer_queue *recv_buffers)
{
	struct buffer *buffer;
	struct list_head *list_node;
	int retval;

	// XXX Check that this is correct
	if (list_empty(recv_buffers->list)) {
		buffer = buffer_new(sizeof(struct request_header));
		if (buffer == NULL) {
			return -1;
		}
		list_node = &(buffer->list_node);
		// XXX We need to enqueue it!
	} else {
		list_node = recv_buffers->list->prev;
		buffer = list_entry(list_node, struct buffer, list_node);
	}

	/* Receive the header first so we know how much to realloc for the 
	   message body because it might be variable length. */
	if (buffer->offset < response_header_length()) {
		retval = recv(socket_fd, buffer->buffer + buffer->offset, 
		              response_header_length() - buffer->offset, 0);
		switch (retval) {
		case 0:
			errno = EHOSTDOWN;
		case -1:
			if (errno == EAGAIN) {
				return 0;
			}
			return -1;
		default:
			buffer->offset += retval;
		}

		if (buffer->offset == response_header_length()) {
			struct response_header *header = (struct response_header *)
				buffer->buffer;
			int length = header->length;
			buffer->buffer = (char *) realloc(buffer->buffer, length);
			// Check for NULL
			buffer->length = length;
		}
	}

	if (buffer->offset >= response_header_length() &&
	    buffer->offset < buffer->length) {
		retval = recv(socket_fd, buffer->buffer + buffer->offset,
		              buffer->length - buffer->offset, 0);
		switch (retval) {
		case 0:
			errno = EHOSTDOWN;
		case -1:
			if (errno == EAGAIN) {
				return 0;
			}
			return -1;
		default:
			buffer->offset += retval;
		}
	}

	return buffer->offset == buffer->length;
}

static void io_handle_read(RgHandle *handle, int interest)
{
	if (!(interest & INTEREST_READ))
		return;

	int retval;

	retval = io_recv(handle->socket_fd, &(handle->recv_buffers));

	switch (retval) {
	case -1:
		// We have an error!!!
		break;
	case 0:
		// We're not done yet
		break;
	case 1:
		// Dequeue the buffer
		// Extract the object
		// Dequeue the corresponding completion
		// Create pending completion
		// Enqueue on pending completions
		// Notify completion thread
		break;
	}

}

static int io_send(int socket_fd, struct buffer_queue *send_buffers)
{
	struct buffer *buffer;
	struct list_head *list_node;
	int retval;

	list_node = send_buffers->list->prev;
	buffer = list_entry(list_node, struct buffer, list_node);

	if (buffer->offset < buffer->length) {
		retval = send(socket_fd, buffer->buffer + buffer->offset,
		              buffer->length - buffer->offset, 0);
		switch (retval) {
		case 0:
			errno = EHOSTDOWN;
		case -1:
			if (errno == EAGAIN) {
				return 0;
			}
			return -1;
			break;
		default:
			buffer->offset += retval;
		}
		
	}

	return buffer->offset == buffer->length;
}

static void io_handle_write(RgHandle *handle, int interest)
{
	if (!(interest & INTEREST_WRITE))
		return;

	int retval;

	if (!(interest & INTEREST_WRITE))
		return;

	retval = io_send(handle->socket_fd, &(handle->send_buffers));
	
	switch (retval) {
	case -1:
		// We have an error!!!
		break;
	case 0:
		// We're not done  yet
		break;
	case 1:
		// Free send buffer
		break;
	}
}

static void *io_main(void *h)
{
	RgHandle *handle = (RgHandle *) h;
	struct epoll_event events[2];
	int max_events;
	int interest;
	int epoll_fd;
	int retval;

	epoll_fd = handle->epoll_fd;

	/* Register self pipe. */
	events[0].data.fd = handle->self_pipe[SELFPIPE_OUT];
	events[0].events = EPOLLIN;
	retval = epoll_ctl(epoll_fd, EPOLL_CTL_ADD, handle->self_pipe[SELFPIPE_OUT],
			   &events[0]);
	if (retval < 0) {
		perror("epoll_ctl");
		return;
	}

	/* Attempt to connect then register socket */
	if (io_connect(handle) < 0) {
		fprintf(stderr, "Cannot connect.");
		return;
	}
	events[1].data.fd = handle->socket_fd;
	events[1].events = EPOLLIN | EPOLLOUT;
	retval = epoll_ctl(epoll_fd, EPOLL_CTL_ADD, handle->socket_fd, &events[1]);
	if (retval < 0) {
		perror("epoll_ctl");
		return;
	}

	max_events = 2;

	/* Main event loop. */
	while (handle->close_requested == 0) {
		interest = 0;
		retval = epoll_wait(epoll_fd, events, max_events, -1);
		if (retval < 0) {
			perror("epoll_wait");
			return;
		}

		/* Check self-pipe to see if we have new input from the 
		   application thread. */
		if (events[0].events & EPOLLIN) {
			drain(handle->self_pipe[SELFPIPE_OUT]);
			interest |= INTEREST_WRITE;
		}

		/* Check events */
		if (events[1].events & EPOLLIN)
			interest |= INTEREST_READ;
		if (events[1].events & EPOLLIN || events[1].events & EPOLLHUP) 
			interest |= INTEREST_WRITE;

		if (interest & INTEREST_READ) {
			io_handle_read(handle, interest);	
		}
		if (interest & INTEREST_WRITE) {
			io_handle_write(handle, interest);
		}
	}
}

int io_wakeup(RgHandle *handle)
{
	int c = '\0';

	return write(handle->self_pipe[SELFPIPE_IN], &c, 1) == 1
		? RG_OK
		: RG_ESYSTEM;
}
int mtio_queue_completion(RgHandle *handle, int type, void *callback,
                          void *callback_data)
{
	struct completion *completion;

	completion = (struct completion *) malloc(sizeof(*completion));
	completion->type = type;
	completion->callback = callback;
	completion->callback_data = callback_data;

	completion_queue_lock(&(handle->outbound_completions));
	list_add(&(completion->list_node), handle->outbound_completions.list);
	completion_queue_unlock(&(handle->outbound_completions));

	return RG_OK;
}

int mtio_queue_buffer(RgHandle *handle, char *packed_buf, 
                      size_t packed_len)
{
	struct buffer *send_buffer;

	send_buffer = (struct buffer *) malloc(sizeof(*send_buffer));
	send_buffer->length = packed_len;
	send_buffer->buffer = packed_buf;

	buffer_queue_lock(&(handle->send_buffers));
	list_add(&(send_buffer->list_node), handle->send_buffers.list);
	buffer_queue_unlock(&(handle->send_buffers));

	return RG_OK;
}

int mtio_next_xid()
{
	return atomic_inc(&xid_counter);
}


int mtio_send(RgHandle *handle)
{
	return io_wakeup(handle);	
}

int mtio_init(RgHandle *handle)
{
	int retval;

	/* Initialize lists. */
	INIT_LIST_HEAD(handle->outbound_completions.list);
	INIT_LIST_HEAD(handle->inbound_completions.list);
	INIT_LIST_HEAD(handle->send_buffers.list);
	INIT_LIST_HEAD(handle->recv_buffers.list);

	/* Create epoll_fd. */
	handle->epoll_fd = epoll_create(2);
	if (handle->epoll_fd < 0) {
		perror("epoll_create");
		return -1;
	}

	/* Self-pipe used for application thread to wake up I/O thread to send
	   something, which may be stuck in epoll_wait(). XXX Verify. */
	if (pipe(handle->self_pipe) < 0) {
		perror("pipe");
		return -1;
	}
	if (set_nonblock(handle->self_pipe[0]) < 0 ||
	    set_nonblock(handle->self_pipe[1]) < 0) {
		fprintf(stderr, "Cannot set self-pipe to nonblock");
		return -1;
	}

	/* Create I/O and event dispatch threads. */
	pthread_t *io_thread = &(handle->threads.io_thread);
	pthread_t *event_thread = &(handle->threads.event_thread);

	retval = pthread_create(io_thread, NULL, io_main, (void *) handle);
	if (retval < 0) {
		perror("pthread_create");
		return -1;
	}
	retval = pthread_create(event_thread, NULL, event_main, (void *) handle);
	if (retval < 0) {
		perror("pthread_create");
		return -1;
	}

	xid_counter = 0;

	return 0;
}

void mtio_close(RgHandle *handle)
{
	close(handle->self_pipe[0]);
	close(handle->self_pipe[1]);
	close(handle->socket_fd);
	close(handle->epoll_fd);

	void *status;

	pthread_join(handle->threads.io_thread, &status);
	pthread_join(handle->threads.event_thread, &status);
}
