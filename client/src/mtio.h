#ifndef MTIO_H_
#define MTIO_H_

#include <ringmaster.h>
#include <stdint.h>

int mtio_init(RgHandle *handle);

int mtio_next_xid();

int mtio_queue_completion(RgHandle *handle, int type, void *callback,
                          void *callback_data);

int mtio_queue_buffer(RgHandle *handle, char *packed_buf, 
                      size_t packed_len);

void mtio_close(RgHandle *handle);

#endif /* MTIO_H_ */
