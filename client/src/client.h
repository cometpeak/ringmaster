#ifndef RINGMASTER_CLIENT_H_
#define RINGMASTER_CLIENT_H

#include "list.h"

struct threads {
	pthread_t io_thread;
	pthread_t event_thread;
};

enum {
	RG_COMPLETION_DATA,
	RG_COMPLETION_STAT
};

struct completion {
	int type;
	void *callback;
	void *callback_data;
	struct list_head list_node;
};

struct completion_queue {
	struct list_head *list;
	pthread_cond_t cond;
	pthread_mutex_t lock;
};

struct buffer {
	uint32_t length;
	uint32_t offset;
	char *buffer;
	struct list_head list_node;
};

struct buffer_queue {
	struct list_head *list;
	pthread_mutex_t lock;
};

#define SELFPIPE_IN  0
#define SELFPIPE_OUT 1

#define INTEREST_READ  0x01
#define INTEREST_WRITE 0x02

struct ringmaster_handle {
	int epoll_fd;
	int socket_fd;
	int self_pipe[2];

	char *host;
	int port;
	struct sockaddr addr;

	int close_requested;
	int timeout;
	int state;
	int refcount;

	struct threads threads;
	struct completion_queue outbound_completions;
	struct completion_queue inbound_completions;
	struct buffer_queue send_buffers;
	struct buffer_queue recv_buffers;
};

#endif /* RINGMASTER_CLIENT_H_ */
