#ifndef MESSAGE_H_
#define MESSAGE_H_

#include <stdint.h>
#include <ringmaster.h>

/* Fixed-size headers. The type and status values follow the error numbers
   and op codes defined in ringmaster.h. Headers are packed and are the first
   12 bytes of any message. */

struct request_header {
	uint32_t xid;
	uint32_t type;
	uint32_t length;
} __attribute__((packed));

struct response_header {
	uint32_t xid;
	uint32_t status;
	uint32_t length;
} __attribute__((packed));

#define request_header_length() \
	sizeof(struct request_header)

#define response_header_length() \
	sizeof(struct response_header)

/* Convenience structs for request messages. Because these messages are variable
   length, we're going to do the old abuse of C weak typing from Postgres where
   we allocate enough memory to hold this struct plus n - 1 bytes for a n-byte
   variable-length body. */

/* Append request and response. */

struct append_request {
	struct request_header header;
	char data[0];
}__attribute__((packed));

struct append_response {
	struct request_header header;
	uint32_t seqnum;
}__attribute__((packed));

struct append_request *append_request_new();
int append_request_set_data(struct append_request **append_request, 
                            const char *buffer, int len);
int append_request_length(struct append_request *append_request);
void append_request_free(struct append_request *append_request);

struct append_response *append_response_new();
int append_response_length(struct append_response *append_response);
void append_response_free(struct append_response *append_response);

/* Retr request and response. */

struct retr_request {
	struct request_header header;
	uint32_t seqnum;
}__attribute__((packed));

struct retr_response {
	struct response_header header;
	char data[0];
}__attribute__((packed));

struct retr_request *retr_request_new();
int retr_request_length(struct retr_request *retr_request);
void retr_request_free(struct retr_request *retr_request);

struct retr_response *retr_response_new();
int retr_response_set_data(struct retr_response **retr_request, 
                           const char *buffer, int len);
int retr_response_length(struct retr_response *retr_response);
void retr_response_free(struct retr_response *retr_response);

/* Disconnect request and response. */

struct disconnect_request {
	struct response_header header;
}__attribute__((packed));

struct disconnect_response {
	struct response_header header;
}__attribute__((packed));

struct disconnect_request *disconnect_request_new();
int disconnect_request_length(struct disconnect_request *disconnect_request);
void disconnect_request_free(struct disconnect_request *disconnect_request);

struct disconnect_response *disconnect_response_new();
int disconnect_response_length(struct disconnect_response *disconnect_response);
void disconnect_response_free(struct disconnect_response *disconnect_response);

#endif
