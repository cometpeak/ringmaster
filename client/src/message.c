#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <ringmaster.h>

#include "message.h"

/* Append request and response */

struct append_request *append_request_new(int body_len)
{
	struct append_request *request;
	int len;

	if (body_len < 0)
		return NULL;

	len = sizeof(struct append_request);
	if (body_len > 1)
		len = body_len - 1;

	request = (struct append_request *) malloc(len);

	return request;
}

int append_request_set_data(struct append_request **append_request,
                            const char *buffer, int len)
{
	if (append_request == NULL || *append_request == NULL)
		return -1;

	if (buffer == NULL)
		return -1;
	
	struct append_request *request = *append_request;
	int request_length;

	if (len < request->header.length)
		return -1;

	if (request->header.length == request_header_length()) {
		request_length = sizeof(struct append_request) + len - 1;
		request = (struct append_request *) realloc(request, request_length);
		if (request == NULL)
			return -1;
	} else {
		assert(request->header.length - request_header_length() == len);
	}

	memcpy((void *) ((char *) request + request_header_length() + 1),
	       buffer, len);

	return 0;
}

int append_request_length(struct append_request *append_request)
{
	return append_request->header.length;
}

void append_request_free(struct append_request *append_request)
{
	free(append_request);
}

struct append_response *append_response_new()
{
	return (struct append_response *) malloc(sizeof(struct append_response));
}

void append_response_free(struct append_response *append_response)
{
	free(append_response);
}

/* Retr request and response */

struct retr_request *retr_request_new()
{
	return (struct retr_request *) malloc(sizeof(struct retr_request));	
}

int retr_request_length(struct retr_request *retr_request)
{
	return retr_request->header.length;
}

void retr_request_free(struct retr_request *retr_request)
{
	free(retr_request);
}

struct retr_response *retr_response_new(int body_len)
{
	struct retr_response *response;
	int len;

	if (body_len < 0)
		return NULL;

	len = sizeof(struct retr_response);
	if (body_len > 1)
		len = body_len - 1;

	response = (struct retr_response *) malloc(len);

	return response;
}

int retr_response_set_data(struct retr_response **retr_response,
                           const char *buffer, int len)
{
	if (retr_response == NULL || *retr_response == NULL)
		return -1;

	if (buffer == NULL)
		return -1;
	
	struct retr_response *response = *retr_response;
	int message_length;

	if (len < response->header.length)
		return -1;

	if (response->header.length == response_header_length()) {
		message_length = sizeof(struct retr_response) + len - 1;
		response = (struct retr_response *) realloc(response, message_length);
		if (response == NULL)
			return -1;
	} else {
		assert(response->header.length - response_header_length() == len);
	}

	memcpy((void *) ((char *) response + response_header_length() + 1),
	       buffer, len);

	return 0;
}

int retr_response_length(struct retr_response *retr_response)
{
	return retr_response->header.length;
}

void retr_response_free(struct retr_response *retr_response)
{
	free(retr_response);
}

/* Disconnect request and response */

struct disconnect_request *disconnect_request_new()
{
	return (struct disconnect_request *) malloc(sizeof(struct disconnect_request));
}

int disconnect_request_length(struct disconnect_request *disconnect_request)
{
	return disconnect_request->header.length;
}

void disconnect_request_free(struct disconnect_request *disconnect_request)
{
	free(disconnect_request);
}

struct disconnect_response *disconnect_response_new()
{
	return (struct disconnect_response *) malloc(sizeof(struct disconnect_response));
}

int disconnect_response_length(struct disconnect_response *disconnect_response)
{
	return disconnect_response->header.length;
}

void disconnect_response_free(struct disconnect_response *disconnect_response)
{
	free(disconnect_response);
}


