
#include <stdint.h>
#include <stdlib.h>

#include "atomic.h"

int32_t atomic_inc(int32_t *counter)
{
	int v;
	int incr = 1;

	v = atomic_fetch_and_add(counter, incr);

	return v + incr;
}

int32_t atomic_dec(int32_t *counter)
{
	int v;
	int decr = -1;

	v = atomic_fetch_and_add(counter, decr);

	return v + decr;
}

int32_t atomic_fetch_and_add(volatile int32_t *operand, int incr)
{
	int32_t result;
	asm volatile (
		"lock xaddl %0,%1"
		: "=r" (result), "=m" (*(int *) operand)
		: "0" (incr)
		: "memory");

	return result;
}

