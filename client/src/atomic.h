#ifndef ATOMIC_H_
#define ATOMIC_H_

/**
 * Atomically increment integer counter.
 *
 * @param counter
 * @return value of counter after increment
 */
int32_t atomic_inc(int32_t *counter);

/**
 * Atomically decrement integer counter.
 *
 * @param counter
 * @return value of counter after decrement
 */
int32_t atomic_dec(int32_t *counter);

/**
 * Atomically perform fetch-and-add of a given value on an integer.
 *
 * @param operand
 * @param incr
 */
int32_t atomic_fetch_and_add(volatile int32_t *operand, int incr);

#endif /* ATOMIC_H_ */
