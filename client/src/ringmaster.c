#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <unistd.h>

#include <ringmaster.h>

#include "common.h"
#include "message.h"
#include "client.h"
#include "debug.h"
#include "mtio.h"

static unsigned long ringmaster_name_resolve(char *host)
{
	struct in_addr addr;
	struct hostent *host_ent;

	if ((addr.s_addr = inet_addr(host)) == (unsigned) -1) {
		host_ent = gethostbyname(host);
		if (host_ent == NULL) {
			perror("gethostbyname");
			return -1;
		}
		memcpy(host_ent->h_addr, (char *) &addr.s_addr, 
		                           host_ent->h_length);
	}

	return addr.s_addr;
}

RgHandle *ringmaster_init(const char *host, int timeout)
{
	int retval;

	/* Allocate and initialize handle. */
	RgHandle *handle = (RgHandle *) malloc(sizeof(*handle));
	if (handle == NULL) {
		fprintf(stderr, "Out of memory: cannout allocate RgHandle.\n");
		return NULL;
	}

	handle->timeout         = timeout;
	handle->state           = RG_STATE_DISCONNECTED;
	handle->refcount        = 0;
	handle->close_requested = 0;

	/* Parse host:port */
	char *sep = strchr(host, ':');
	*sep = '\0';

	char *hostname = strdup(host);
	char *portnum  = strdup(sep + 1);

	handle->host = hostname;
	handle->port = atoi(portnum);
	free(portnum);

	/* Look up address. */
	struct sockaddr_in *sin = (struct sockaddr_in *) &handle->addr;
	memset(sin, 0, sizeof(struct sockaddr_in));
	sin->sin_family      = AF_INET;
	sin->sin_port        = htons(handle->port);
	sin->sin_addr.s_addr = ringmaster_name_resolve(handle->host);
	if (sin->sin_addr.s_addr == NULL)
		return NULL;

	/* Start threads. */
	retval = mtio_init(handle);
	if (retval < 0) {
		fprintf(stderr, "Failed to initialize ringmaster");
		return NULL;
	}

	return handle;
}

int ringmaster_append(RgHandle *handle, const char *buffer, size_t len,
                      RgStatCallback callback, void *callback_data)
{
	struct append_request *append_request;

	if (handle == NULL || buffer == NULL || callback == NULL)
		return RG_EBADARG;

	if (len < 0)
		return RG_EBADARG;

	append_request = append_request_new(len);
	if (append_request == NULL)
		return RG_ESYSTEM;

	append_request->header.xid = mtio_next_xid();
	if (append_request_set_data(&append_request, buffer, len) < 0 )
		return RG_ESYSTEM;

	mtio_queue_completion(handle, RG_COMPLETION_STAT, callback, 
	                      callback_data);
	mtio_queue_buffer(handle, (char *) append_request,
	                  append_request_length(append_request));
	mtio_send(handle);
	
	return RG_OK;
}


int ringmaster_retr(RgHandle *handle, int seqnum, RgDataCallback callback,
                    void *callback_data)
{
	struct retr_request *retr_request;

	if (handle == NULL || callback == NULL)
		return RG_EBADARG;

	retr_request = retr_request_new();
	if (retr_request == NULL)
		return RG_ESYSTEM;

	retr_request->header.xid = mtio_next_xid();
	retr_request->seqnum = seqnum;

	mtio_queue_completion(handle, RG_COMPLETION_DATA, callback,
	                      callback_data);
	mtio_queue_buffer(handle, (char *) retr_request, 
	                  retr_request_length(retr_request));
	mtio_send(handle);

	return RG_OK;
}

int ringmaster_close(RgHandle *handle)
{
	close(handle->epoll_fd);
	mtio_close(handle);
	free(handle);
}

