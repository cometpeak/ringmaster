package main

import (
  "net"
  "fmt"
  "flag"
  "log"
)


func main () {
  for a, b := range []string{"a", "b", "c", "d", "e"}[2:4] {
    println(a, b)
  }
  destAddr := flag.String("dest", "", "The destination host address")
  flag.Parse()
  conn, err := net.Dial("tcp", fmt.Sprintf("%s:%d", *destAddr, 5555))
  if err != nil {
    log.Fatal(err)
  }
  fmt.Fprintln(conn, "hello\nderp")
}


