package main

import (
	"flag"
	"fmt"
	"io"
	"net"
	"ringmaster/server"
	// "ringmaster/server/derp"
	"ringmaster/server/consensus"
	"strconv"
	"strings"
)

func main() {
	var _ consensus.ClassicPaxos = consensus.ClassicPaxos{}

	flagLocalPort := flag.Int("localPort", 50000, "The destination host address")
	flagPeers := flag.String("peers", "localhost:51111", "Comma delimited list of hostname:port")
	flag.Parse()

	ntwk := new(server.NetworkImpl)
	peers := strings.Split(*flagPeers, ",")
	args := make([]string, len(peers)+1)

	args[0] = strconv.Itoa(*flagLocalPort)
	for i, v := range peers {
		args[i+1] = v
	}
	fmt.Printf("%v\n", args)

	ntwk.Setup(args)
	//ntwk.Setup([]string{strconv.Itoa(*flagLocalPort), "localhost:51111", "localhost:52222"})
}

func handleConnection(conn net.Conn) {
	fmt.Println("derp")
	io.Copy(conn, conn)
	conn.Close()
}
